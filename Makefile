KUBE_NAMESPACE?=testnamespace
CI_COMMIT_SHA?=local
MINIKUBE?=false
HELM_RELEASE_PVC?=dashboard-pvc1
HELM_RELEASE?=dashboard1
HELM_CHARTS?=ska-tango-taranta-dashboard ska-tango-taranta-dashboard-pvc
K8S_CHART?=ska-tango-taranta-dashboard
# Fixed variables
TIMEOUT = 86400
PROJECT=ska-tango-taranta-dashboard

JS_PROJECT_DIR ?= ./taranta-dashboard
JS_PACKAGE_MANAGER ?= npm
JS_ESLINT_CONFIG ?= .eslintrc.json
JS_JEST_SWITCHES ?=
JS_SRC = "."

# Docker and Gitlab CI variables
RDEBUG ?= ""
CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= job$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=4 count=1 2>/dev/null;echo)
GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REPOSITORY_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
GITLAB_USER_EMAIL ?= "nobody@example.com"
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
DOCKERFILE ?= Dockerfile
EXECUTOR ?= docker
STAGE ?= build_react_artefacts

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#

# include core make support
include .make/base.mk

# include OCI Images support
include .make/oci.mk

# include k8s support
include .make/k8s.mk

# include Helm Chart support
include .make/helm.mk

# Include Python support
include .make/python.mk

# include raw support
include .make/raw.mk

# include js support
include .make/js.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

oci-pre-build:
	@echo "Updating taranta-dashboard configuration..."
	@sed -i "/elastic_disableElasticQuery/s/: true/: $(ELASTIC_DISABLE)/" taranta-dashboard/config.json
	@sed -i "s|\\(\"elastic_baseUrl\"\\s*:\\s*\\)\"\"|\\1\"$(ELASTIC_BASE_URL)\"|" taranta-dashboard/config.json
	@sed -i "s|\\(\"elastic_caCertificate\"\\s*:\\s*\\)\"\"|\\1\"$(ELASTIC_CA_CERTIFICATE)\"|" taranta-dashboard/config.json
	@sed -i "s|\\(\"elastic_clientCertificate\"\\s*:\\s*\\)\"\"|\\1\"$(ELASTIC_CLIENT_CERTIFICATE)\"|" taranta-dashboard/config.json
	@sed -i "s|\\(\"elastic_clientKey\"\\s*:\\s*\\)\"\"|\\1\"$(ELASTIC_CLIENT_KEY)\"|" taranta-dashboard/config.json
	@sed -i "s|\\(\"elastic_apiKey\"\\s*:\\s*\\)\"\"|\\1\"$(ELASTIC_API_KEY)\"|" taranta-dashboard/config.json
	@echo "Configuration update complete."

watch:
	@watch kubectl get pod -n $(KUBE_NAMESPACE);

namespace:
	@kubectl create namespace $(KUBE_NAMESPACE) || kubectl describe namespace $(KUBE_NAMESPACE)

install_all: install_dashboard_pvc install_dashboard

install_dashboard_pvc: namespace
	@helm install $(HELM_RELEASE_PVC) -n $(KUBE_NAMESPACE) charts/ska-tango-taranta-dashboard-pvc/ \
	--set global.minikube=true

install_dashboard: namespace
	@helm dependency update charts/ska-tango-taranta-dashboard/ && \
	helm install $(HELM_RELEASE) -n $(KUBE_NAMESPACE) charts/ska-tango-taranta-dashboard/ \
	--set global.minikube=true

uninstall_all: uninstall_dashboard uninstall_dashboard_pvc

uninstall_dashboard_pvc:
	@helm uninstall $(HELM_RELEASE_PVC) -n $(KUBE_NAMESPACE)

uninstall_dashboard:
	@helm uninstall $(HELM_RELEASE) -n $(KUBE_NAMESPACE) || true

js-test-local:
	@docker rm -f mongo-test || true
	docker run --rm --name mongo-test -d -p 27017:27017 mongo:latest
	# Optionally wait for MongoDB to be ready
	sleep 5
	export JS_JEST_SWITCHES="--runInBand"
	make js-test
	docker stop mongo-test
